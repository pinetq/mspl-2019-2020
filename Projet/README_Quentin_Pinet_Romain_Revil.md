# Groupe
- Quentin Pinet
- Romain Revil

# Lien du jeu de données
https://databank.banquemondiale.org/source/world-development-indicators#

# Questions proposées
- Comment a évolué l'IDH selon les pays depuis 1960?
- Comment ont évolué le PIB et l'espérance de vie depuis 1960?

# Question retenue
Comment ont évolué le PIB et l'espérance de vie depuis 1960?

# Remarques éventuelles
Nous avons choisi ce data set car le sujet nous intéressait et ces données sont relativement complètes et d'une source fiable.
Il contient les différentes valeurs nécessaires au calcul de l'IDH (PIB par habitant, espérance de vie à la naissance et niveau d'éducation) pour tous les pays du monde des années 1960 à 2019.